# Introduction à R et bonnes pratiques de la recherche

Bonjour,

Vous trouverez ici les ressources du cours "Introduction à R et bonnes pratiques de la recherche" **donné dans le cadre du programme doctoral en études numériques** de swissuniversities à l'Université de Lausanne, 22 novembre 2018, par **Jean-Pierre Mueller** et **Yannick Rochat**.

Le cours est divisé en trois parties.

1. Introduction à R (première partie)
2. Bonnes pratiques dans la gestion, le traitement et la publication des données de la recherche
3. Introduction à R (deuxième partie)

Dans ce dépôt (en anglais: *repository*), vous trouverez pour chacun de ces trois sujets un notebook un des slides. Il s'agit exactement du même contenu entre un notebook et les slides crrrespondantes, car les secondes ont été générées à partir du premier.

Cependant, un notebook ouvert dans RStudio est alors interactif. Vous pouvez modifier le code et le réévaluer directement dans le notebook, sans passer par la console.